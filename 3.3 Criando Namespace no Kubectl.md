
#### Criando Namespace na linha de comando:
O projeto receberá um ID que pode ser identificado facilmente, clique para acessar o projeto e na URL, será apresentado algo como "c-zpqxm:p-btxmb".

Para criar um Namespace neste projeto, basta adicionar este parâmetro no annotations do manifesto YAML da seguinte forma:
```yaml
apiVersion: v1
kind: Namespace
metadata:
  name: ns-lab
  annotations:
    field.cattle.io/projectId: c-zpqxm:p-btxmb
```

O Namespace agora pode ser visualizado com um comando Kubernetes na CLI
```bash
kubectl get ns
``` 