**Executar no node extras**
Instalar RKE
```bash
curl -LO https://github.com/rancher/rke/releases/download/v1.4.7/rke_linux-amd64
mv rke_linux-amd64 rke
chmod +x rke
sudo mv ./rke /usr/local/bin/rke
rke --version
```

**COPIAR este arquivo**
`vim rancher-cluster.yml`
```yaml
nodes:
  - address: 192.168.1.201
    user: ubuntu
    role: [controlplane, worker, etcd]
  - address: 192.168.1.202
    user: ubuntu
    role: [controlplane, worker, etcd]
  - address: 192.168.1.203
    user: ubuntu
    role: [controlplane, worker, etcd]

services:
  etcd:
    snapshot: true
    creation: 6h
    retention: 24h

# Required for external TLS termination with
# ingress-nginx v0.22+
ingress:
  provider: nginx
  options:
    use-forwarded-headers: "true"
```

#### Incluir o DNS do rancher em todos os nodes
`sudo vim /etc/hosts`
```
192.168.1.201 node1.exemplo-ha.com
192.168.1.202 node2.exemplo-ha.com
192.168.1.203 node3.exemplo-ha.com
192.168.1.204 rancher.exemplo-ha.com
```

**Executar no node extras** a implantação do rke
```bash
rke up --config ./rancher-cluster.yml
```

**Após o cluster subir, vamos gerenciar o Kubernetes**
```bash
export KUBECONFIG=$(pwd)/kube_config_rancher-cluster.yml
```
ou 
```bash
mkdir .kube
cp kube_config_rancher-cluster.yml .kube/config
```

Ver a situação do cluster
```bash
kubectl get nodes
kubectl get pods --all-namespaces
```
