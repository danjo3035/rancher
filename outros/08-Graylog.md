### Criação

Execute o arquivo **graylog.yaml**
```bash
kubectl create -f arquivos/graylog.yaml
```

#### Criar uma entrada de logs
- Clique em **System** -> **Inputs**
- Selecione Gelf UDP e clique no botão Launch new unput
- na aba frontal que aparece, selecione **Global**, dê o título **k8s-** e clique em **Save**

#### Extrair dos logs o que nos interessa
- Clique no botão **Manager extractors**.
- Clique no botão **Get started**
- Clique no botão abaixo **Load Message**
- Escolher a mensagem do tipo **kubernetes** e no selext extrator type, escolher o tipo **Json**
- Em **Key prefix**, vamos inserir o prefixo **k8s**
- Em **Extractor title** vamos isnerir também **k8s-**
- Clique em **Create extractor**

Vá ao menu **Search** 
